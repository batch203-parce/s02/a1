I joined the bootcamp because I wanted to broaden my knowledge and skills in software/web development. 
I have always been interested in this field since I am in Senior High School.

As for my work experience as a student, I developed a static website of our former school or Alma Mater.
I also developed a mobile application with automatic image recognition that can identify 10 local mangrove species for our Capstone project.
I am also knowledgeable in Adobe Photoshop and sometimes do a freelance work as a photo editor and layout artist.